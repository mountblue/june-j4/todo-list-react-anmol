const express = require("express"),
  bodyParser = require("body-parser"),
  path = require("path"),
  app = express(),
  mongoose = require("mongoose"),
  url = "mongodb://localhost:27017/todos-db";

//connect db
mongoose.connect(
  url,
  {
    useNewUrlParser: true
  }
);
mongoose.Promise = global.Promise;

app.use(bodyParser.json());
app.use(express.static("../client/dist"));

//initialize routes
app.use("/api", require("./routes/api"));

//error handling middleware
app.use(function(err, req, res, next) {
  // console.log(err);
  res.status(422).send({ error: err.message });
});

app.get("/", function(req, res) {
  res.sendFile(path.resolve("../client/src/index.html"));
});

app.listen(process.env.port || "5000", "localhost", function() {
  console.log("Started the server");
});
