const mongoose = require("mongoose"),
  Schema = mongoose.Schema;

//create schema
const TodoSchema = new Schema({
  _id: {
    type: String,
    required: [true, "Id field is required"]
  },
  task: {
    type: String,
    required: [true, "Task field is required"]
  },
  status: {
    type: Boolean,
    default: false
  }
});

const Todos = mongoose.model("todos", TodoSchema);

module.exports = Todos;
