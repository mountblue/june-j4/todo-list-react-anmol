const express = require("express"),
  router = express.Router(),
  Todos = require("../models/todos");

//get todos list data from the database
router.get("/todos", function(req, res, next) {
  Todos.find({})
    .then(function(todos) {
      res.send(todos);
    })
    .catch(next);
});

//add todos data in the database
router.post("/todos", function(req, res, next) {
  Todos.create(req.body)
    .then(function(todos) {
      res.send(todos);
    })
    .catch(next);
});

//get todos list data by id from the database
router.get("/todos/:id", function(req, res, next) {
  Todos.findById({ _id: req.params.id })
    .then(function(todos) {
      res.send(todos);
    })
    .catch(next);
});

//update todos data from the database
router.put("/todos/:id", function(req, res, next) {
  Todos.findByIdAndUpdate({ _id: req.params.id }, req.body).then(function() {
    Todos.findOne({ _id: req.params.id }).then(function(todos) {
      res.send(todos);
    });
  });
});

//delete todos data from the database
router.delete("/todos/:id", function(req, res, next) {
  Todos.findByIdAndRemove({ _id: req.params.id }).then(function(todos) {
    res.send(todos);
  });
});

module.exports = router;
