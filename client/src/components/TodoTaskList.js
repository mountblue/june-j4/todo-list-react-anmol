import React from "react";

function TodoTaskList(props) {
  const items = props.items;
  const listItems = items.map(
    item =>
      item.status === false ? (
        <div className="listItemsDiv">
          <li key={item._id}>{item.task}</li>
          <span>x</span>
        </div>
      ) : (
        <div className="listItemsDiv">
          <li key={item._id} className="lineThrough">
            {item.task}
          </li>
          <span>x</span>
        </div>
      )
  );
  let list = function(event) {
    if (event.target.textContent === "x") {
      let node = Array.prototype.slice.call(
        event.target.parentNode.parentNode.children
      );
      let deleteNode = node.indexOf(event.target.parentNode);
      props.deleteTask(deleteNode);
    } else {
      var target = $(event.target);
      if (target.is("li")) {
        let node = Array.prototype.slice.call(
          event.target.parentNode.parentNode.children
        );
        let checkNode = node.indexOf(event.target.parentNode);
        props.taskComplete(checkNode);
      }
    }
  };
  return (
    <div className="TaskListDiv">
      <ul className="TaskListUl" onClick={list}>
        {listItems}
      </ul>
    </div>
  );
}

export default TodoTaskList;
