import React from "react";

function TodoInput(props) {
  let handleEnter = function(event) {
    if (event.key === "Enter") {
      props.addTask(event.target.value);
      event.target.value = "";
    }
  };

  return (
    <div>
      <div>
        <input
          className="taskInput"
          type="text"
          placeholder="add a new task"
          onKeyPress={handleEnter}
        />
      </div>
      <div />
    </div>
  );
}
export default TodoInput;
