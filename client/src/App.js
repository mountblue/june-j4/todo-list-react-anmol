import React, { Component } from "react";
import TodoInput from "./components/TodoInput";
import TodoTaskList from "./components/TodoTaskList";
import "./assets/styles/App.css";
const uuidv1 = require("uuid/v1");

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      todo: []
    };
    this.addTask = this.addTask.bind(this);
    this.taskComplete = this.taskComplete.bind(this);
    this.deleteTask = this.deleteTask.bind(this);
  }

  addTask(data) {
    const id = uuidv1();
    // console.log(id)
    if (data !== "" && data.replace(/\s/g, "").length) {
      // console.log(id)
      fetch("/api/todos", {
        method: "POST",
        body: JSON.stringify({
          task: data,
          status: false,
          _id: id
        }),
        headers: {
          "Content-Type": "application/json"
        }
      })
        .then(res => res.json())
        .catch(error => console.error("Error:", error));
      this.setState({
        todo: [
          ...this.state.todo,
          {
            task: data,
            status: false,
            _id: id
          }
        ]
      });
    }
  }

  taskComplete(index) {
    // console.log(index);
    let newTodo = [...this.state.todo];
    if (newTodo[index].status === false) {
      newTodo[index].status = true;
    } else {
      newTodo[index].status = false;
    }
    fetch(`/api/todos/${newTodo[index]._id}`, {
      method: "PUT",
      body: JSON.stringify({
        status: true
      }),
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(res => res.json())
      .catch(error => console.error("Error:", error));
    this.setState({ todo: newTodo });
  }

  deleteTask(index) {
    let newTodo = this.state.todo;
    let itemId = newTodo[index]._id;

    fetch(`/api/todos/${newTodo[index]._id}`, {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(res => res.json())
      .catch(error => console.error("Error:", error));

    newTodo = this.state.todo.filter(function(todo) {
      return todo._id !== itemId;
    });
    this.setState({ todo: newTodo });
  }

  componentWillMount() {
    fetch("/api/todos/")
      .then(res => res.json())
      .then(
        data => {
          this.setState({
            todo: data
          });
          // console.log(data)
        },
        error => {
          console.log(error);
        }
      );
  }

  render() {
    return (
      <div className="App">
        <div className="AppHeader">Todo List</div>
        <section className="InputSection">
          <TodoInput addTask={this.addTask} />
          <TodoTaskList
            items={this.state.todo}
            taskComplete={this.taskComplete}
            deleteTask={this.deleteTask}
          />
        </section>
      </div>
    );
  }
}

export default App;
